Nous vous proposons de commencer ce module par la lecture de la quatrième partie de l'ouvrage de Hartmann _etal_. Les auteurs nous présentent plusieurs méthodes d'enseignement : la pédagogie expérientielle, le travail de groupe, les programmes dirigés, les apprentissages par la découverte ou encore la pédagogie de projet. 

Autant de méthodes qui peuvent vous donner des idées pour proposer, en complément de votre activité initiale, une autre activité, individuelle ou collective.

Cette lecture pourra se compléter de la cinquième partie qui aborde les techniques d'enseignement. Parmi ces techniques l'une consiste à rendre concrètes des notions abstraites par le jeu des représentations énactiques, iconiques et symboliques.
