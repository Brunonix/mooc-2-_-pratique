# Accompagner l'individu et le collectif

Dans les séquences précédentes, nous avons créé des activités pour les élèves en détaillant la mise en oeuvre en classe. Dans cette séquence, il va s'agir de préciser comment accompagner chaque élève dans son apprentissage.

Qu'il soit en difficulté ou en avance, chaque élève est singulier et demande souvent de proposer des activités supplémentaires, de remédiation ou d'explorations complémentaires.

Une fois notre activité choisie, créée, sa mise en oeuvre soigneusement orchestrée, nous pouvons accompagner plus avant les élèves : ceux et celles qui ont pris de l'avance, ceux et celles qui semblent en diffculté, l'ensemble de la classe en proposant par exemple un réinvestissement des apprentissages lors d'un projet.


## Travail demandé

Cette partie se structure différement des précédentes, nous ne sommes plus dans une démarche de "modélisation" mais de propositions concrètes pour faire face aux défis usuels qui peuvent se poser.

Plusieurs types d'activité peuvent constituer le contenu de cette troisième séquence. On pourra par exemple :

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)
- Proposer une activité pour la découverte ou la consolidation d'un concept qui autorise des cheminements différents pour atteindre un même objectif (voir par exemple l'activité sur les types abstraits de données).

L'activité produite pour cette séquence pourra soit faire référence à des activités proposées aux séquences 1 et 2 soit être sans lien. Dans tous les cas, il faudra bien préciser non seulement la nature de l'activité mais également son contexte.


## Boîte à outils

Pour vous préparer vous aurez à votre disposition : 

- La quatrième partie, du livre "Enseigner l'Informatique" de Werner Hartmann, suivi du quiz-bloc2 n°3

- La cinquième partie, du livre "Enseigner l'Informatique" de Werner Hartmann, suivi du quiz-bloc2 n°4

- 4 ressources de collégues pour vous inspirer et discuter sur le forum.

- La to do liste de fin, avec les actions à réaliser pour répondre aux objectifs ET pour bien se préparer pour l'évaluation par les pairs.

Et bien sur la [communauté d’apprentissage et de pratique grâce au forum](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/accompagner/194) qui fait converger le forum de la communauté CAI (Communauté d'Apprentissage de l'Informatique) et les forums des Moocs NSI.

